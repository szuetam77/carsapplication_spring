package pl.cars.model;

import java.util.ArrayList;
import java.util.List;

public class Category {

    private long id;
    private String name;
    private String description;


    public Category() {
    }

    public Category(long id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    private static List<Category> carsList = new ArrayList<>();


    static {
        Category sport = new Category(0L, "Sport", "Opis kategorii sport");
        Category SUV = new Category(1L, "SUV", "Opis kategorii SUV");
        Category daily = new Category(2L, "Daily", "Opis kategorii daily");
        carsList.add(sport);
        carsList.add(SUV);
        carsList.add(daily);

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public static List<Category> getCarsList() {
        return carsList;
    }

    public static void setCarsList(List<Category> carsList) {
        Category.carsList = carsList;
    }
}
