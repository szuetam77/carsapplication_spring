package pl.cars.model;

import java.util.ArrayList;
import java.util.List;

public class HomePage {

    private long id;
    private String name;

    private static List<HomePage> list = new ArrayList<>();

    public HomePage(String name) {
        this.name = name;
    }

    public HomePage(){

    }

    public HomePage(String name, HomePage homePage){
        this.id = id;
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static List<HomePage> getList() {
        return list;
    }

    public static void setList(List<HomePage> list) {
        HomePage.list = list;
    }

    public void setHomePage(HomePage homePage) {this.setHomePage(homePage);
    }
    public static List<HomePage> homePages(){
        return homePages();
    }
}
