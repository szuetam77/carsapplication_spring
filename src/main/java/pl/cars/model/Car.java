package pl.cars.model;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Car {

    private long id;
    private String name;
    private String model;
    private int hp;
    private Category category;

    private static List<Car> list = new ArrayList<>();


    public Car(long id, String name, String model, int hp, Category category) {
        this.id = id;
        this.name = name;
        this.model = model;
        this.hp = hp;
        this.category = category;
    }

    public Car() {
    }

        static {
        Category sport = Category.getCarsList().get(0);
        Category SUV = Category.getCarsList().get(1);
        Category daily = Category.getCarsList().get(2);


        list.add(new Car(0L, "Ferrari", "488 GTB", 670, sport));
        list.add(new Car(1L, "Mercedes", "GLE 6.3 AMG", 580, SUV));
        list.add(new Car(2L, "Porsche", "Panamera GTS", 680, daily));



    }

    public static List<Car> getCars(){
        return list;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public static List<Car> getCarsByCategory(String name){
        return list.stream().filter(cars -> cars.getCategory().getName().equals(name)).collect(Collectors.toList());
    }

    public static List<Car> getCarsByName(String name){
        return list.stream().filter(cars -> cars.getName().equals(name)).collect(Collectors.toList());
    }

    public static List<Car> getCarsByModel(String model){
        return list.stream().filter(cars -> cars.getModel().equals(model)).collect(Collectors.toList());
    }




}
