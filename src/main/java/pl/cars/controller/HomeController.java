package pl.cars.controller;


import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import pl.cars.model.Car;
import pl.cars.model.Category;
import pl.cars.model.HomePage;

import java.util.List;

@Controller
@RequiredArgsConstructor
public class HomeController {

    @GetMapping("/")
    public String home(ModelMap map, Model model){
        map.put("cars", HomePage.getList());
        return "home";
    }

    @GetMapping("/categories")
    public String categories(ModelMap map){
        map.put("categories", Category.getCarsList());
        return "categories";
    }

    @GetMapping("/category/{id}")
    public String category (@PathVariable int id, ModelMap map){
        Category category = Category.getCarsList().get(id);
        map.put("category", category);
        List<Car> cars = Car.getCarsByCategory(category.getName());
        map.put("cars", Car.getCarsByCategory(category.getName()));
        return "/category";
    }

    @GetMapping("/all-cars")
    public String showAllCars(ModelMap map){
        map.put("cars", Car.getCars());
        return "all-cars";
    }

    @GetMapping("/details/{id}")
    public String showDetails (@PathVariable int id, ModelMap map){

        Car car = Car.getCars().get(id);
        map.put("car", car);
        List<Car> cars = Car.getCarsByModel(car.getModel());
        map.put("cars", Car.getCarsByModel(car.getModel()));
        return "/details";
    }

}
